# Parser for SQAaaS reports

## Description

This repository scrypt to parse SQAaaS reports in json into a table.

If you need to remove blanks from filenames use:

```bash
for f in *\ *; do mv "$f" "${f// /_}"; done
```
