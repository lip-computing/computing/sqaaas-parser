#!/usr/bin/env python3

# -*- encoding: utf-8 -*-
#
# Copyright 2024 LIP
#
# Author: Mario David <mariojmdavid@gmail.com>
#

"""Parses SQAaaS reports and produces a table with list of components
"""

import sys
import os
import json
import csv


# QC categories
# qc_categ = ['QC.Acc', 'QC.Wor', 'QC.Man', 'QC.Doc', 'QC.Lic', 'QC.Ver', 'QC.Sty', 'QC.Met', 'QC.Uni', 'QC.Sec']

# Mandatory Quality Criteria Attributes
mandatory_qc = ['QC.Acc01', 'QC.Acc02', 'QC.Wor01', 'QC.Man01', 'QC.Ver01',
                'QC.Lic01', 'QC.Lic01.1', 'QC.Doc02', 'QC.Doc03', 'QC.Doc04', 'QC.Doc05',
                'QC.Doc06.1', 'QC.Doc06.5', 'QC.Doc06.6', 'QC.Doc06.7',
                'QC.Del01', 'QC.Del02', 'QC.Dep01']

# Bronze badge: include all mandatory QC
bronze_qc = ['QC.Doc01.1', 'QC.Doc06.2', 'QC.Doc06.3']

# Silver badge: include all bronze badge QC
silver_qc = ['QC.Met01']

# Gold badge: include all silver badge QC
gold_qc = ['QC.Sty01', 'QC.Uni01', 'QC.Sec02']


def get_jfiles(projdir):
    '''Get all SW components from the directory file list
    project = itwin or ai4os
    '''
    return os.listdir(projdir)


def parse(projdir, jfile):
    '''Parse json file: information in a SQAaaS report'''
    full_filename = projdir + '/' + jfile
    with open(full_filename) as jfd:
        sw_report = json.load(jfd)

    sw_comp = jfile.replace('.json', '').split('_')[-1]
    check_qc = {}
    for categ in sw_report['report']:
        for subcat in sw_report['report'][categ]['subcriteria']:
            check_qc[subcat] = sw_report['report'][categ]['subcriteria'][subcat]['valid']
#            print(subcat, sw_report['report'][categ]['subcriteria'][subcat]['valid'])

    check_qc['QC.Acc02'] = True
    check_qc['QC.Man01'] = True
    for qc in ['QC.Wor01', 'QC.Doc02', 'QC.Doc03', 'QC.Doc04', 'QC.Doc05', 'QC.Doc06.5', 'QC.Doc06.6', 'QC.Doc06.7', 'QC.Del01', 'QC.Del02', 'QC.Dep01']:
        check_qc[qc] = 'Manual'

    return sw_comp, check_qc


if __name__ == '__main__':

#    projdir = 'json/itwin'
    projdir = 'json/ai4os'

    # all_qc True print all QC, False print only failed QC
    all_qc = True

    jfiles = get_jfiles(projdir)
    for jfile in jfiles:
        (sw_comp, chk_qc) = parse(projdir, jfile)
        print(80*'_')
        print('Mandatory criteria:', sw_comp)
        for qc in mandatory_qc:
            if all_qc:
                print(qc, chk_qc[qc])
            else:
                if not chk_qc[qc]:
                    print(qc, chk_qc[qc])
